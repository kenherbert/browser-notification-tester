# Browser Notification Tester

A tool to test notification functionality in your browser

Please note that this was made for a section of the HTML code to be copy/pasted into a flatpage in my [Django portfolio](https://gitlab.com/firedancersoftware/developer-portfolio) but is setup as a standalone page with no requirement for Python or Django for completeness.